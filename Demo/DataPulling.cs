﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Demo
{
    class DataPulling
    {
        private string ipAddress;
        private int port;
        private int htaID;
        private int status;
        private int htaHandle;
        private int row;
        private DataGridView gvwHTAList;

        //public DataPulling(string ipAddress, int port, int row, gvwHTAList)
        public DataPulling(int i, DataGridView gvwHTAList)
        {
            this.ipAddress = ipAddress;
            this.port = port;
            status = 0;
        }

        public void htaConnect()
        {
            try
            {
                gvwHTAList.EndEdit();
                htaHandle = 0;
                int resultCode;
                resultCode = THTA830DLL.HUNHTAOpenSocket(ref htaHandle, ipAddress, port);
                if (resultCode == 0)
                {
                    gvwHTAList.Rows[row].Cells[3].Value = "Connected";
                    gvwHTAList.Rows[row].Cells[4].Value = "Open " + address + " OK";
                    gvwHTAList.Rows[row].Cells[5].Value = htaHandle;
                }
                else
                {
                    gvwHTAList.Rows[row].Cells[3].Value = "Not Connected";
                    gvwHTAList.Rows[row].Cells[4].Value = "Error connecting! return:" + resultCode.ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void htaDisconnect(object sender, EventArgs e)
        {
            try
            {
                int resultCode;
                resultCode = THTA830DLL.HUNHTACloseSocket(htaPort);
                if (resultCode == 0)
                {
                    gvwHTAList.Rows[this.row].Cells[3].Value = "Disconnected";
                    gvwHTAList.Rows[this.row].Cells[4].Value = "OK";
                }
                else
                {
                    gvwHTAList.Rows[this.row].Cells[4].Value = "Disconnect error: " + resultCode.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void pullData(int htaPort, int id)
        {

            int tmpPrev = _LastRecord;
            _LastRecord = 0;
            byte[] tmpBuffer = new byte[2048];
            int returnCode = 0;
            int resultCode = -1;
            //resultCode = THTA830DLL.HTA850PollingData(htaHandle, tmpPrev, tmpBuffer, ref _LastRecord, ref returnCode, 10000);
            resultCode = THTA830DLL.HTA850PollingData(htaPort, tmpPrev, tmpBuffer, ref _LastRecord, ref returnCode, 10000);
            if (returnCode == 0 && _LastRecord > 0)
            {
                HTA860Return_TextBox.Text += String.Format("IP Addr: "+ this.ipAddress.ToString() +"Record Count: {0}\r\n", _LastRecord.ToString());

                string sDatumVreme = "";
                string sBrojKartice = "";
                string sDogadjaja = "";

                for (int i = 0; i < _LastRecord; i++)
                {
                    sDatumVreme = "";
                    sBrojKartice = "";
                    sDogadjaja = "";
                    int tmpIndex = (i * 41);

                    //StringBuilder tmpSB = new StringBuilder();
                    StringBuilder tmpSBDatumVreme = new StringBuilder();
                    StringBuilder tmpSBBrojKartice = new StringBuilder();
                    //tmpSB.Append("Time:");
                    for (int j = 0; j < 20; j++)
                    {
                        if (tmpBuffer[tmpIndex + j] == 0x00 || tmpBuffer[tmpIndex + j] == 0xFF)
                            continue;
                        //tmpSB.Append(Convert.ToChar(tmpBuffer[tmpIndex + j]));
                        tmpSBDatumVreme.Append(Convert.ToChar(tmpBuffer[tmpIndex + j]));
                    }
                    sDatumVreme = tmpSBDatumVreme.ToString();

                    //tmpSB.Append(", Reader:").Append(tmpBuffer[tmpIndex + 20].ToString("X2"));
                    //tmpSB.Append(", Input Type:").Append(tmpBuffer[tmpIndex + 21].ToString("X2"));
                    //tmpSB.Append(", Section:").Append(tmpBuffer[tmpIndex + 22].ToString("X2"));
                    sDogadjaja = tmpBuffer[tmpIndex + 22].ToString("X2");
                    //tmpSB.Append(", Class:").Append(tmpBuffer[tmpIndex + 23].ToString("X2"));
                    //tmpSB.Append(", Event Code:").Append(tmpBuffer[tmpIndex + 24].ToString("X2"));

                    //tmpSB.Append(", Card:");
                    for (int j = 25; j < 41; j++)
                    {
                        if (tmpBuffer[tmpIndex + j] == 0x00 || tmpBuffer[tmpIndex + j] == 0xFF)
                            continue;
                        //tmpSB.Append(Convert.ToChar(tmpBuffer[tmpIndex + j]));
                        tmpSBBrojKartice.Append(Convert.ToChar(tmpBuffer[tmpIndex + j]));
                    }
                    sBrojKartice = tmpSBBrojKartice.ToString();

                    //tmpSB.Append("\r\n");
                    //SaveToBase(sDatumVreme, sBrojKartice, id, sDogadjaja);
                    HTA860Return_TextBox.Text += sDatumVreme.ToString() + ',' + sBrojKartice.ToString() + ',' + id.ToString() + ',' + sDogadjaja.ToString();
                    //HTA860Return_TextBox.Text += tmpSB.ToString();
                }
                //setCurrentTime(htaPort, id);
                // pull data again !
                pullData(htaPort, id);
            }
            else
            {
                HTA860Return_TextBox.Text += String.Format("Result Code: {0}\r\n", resultCode.ToString());
                HTA860Return_TextBox.Text += String.Format("Return Code: 0x{0}\r\n", returnCode.ToString("X4"));
                // The HTA-860 has no data
                if (resultCode == 1010)
                    HTA860Return_TextBox.Text += String.Format("The HTA-860: " + address + " has no data!\r\n");
            }
            
        }
        private void setCurrentTime(int htaPort, int id)
        {
            try
            {
                int resultCode = -1;
                uint timeout = 1000;
                string date, time;
                date = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + Convert.ToInt16(DateTime.Now.DayOfWeek).ToString();
                time = DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
                resultCode = THTA830DLL.HUNWriteHTADateTime(htaPort, 1, date, time, timeout);
                if (resultCode == 0)
                {
                    HTA860Return_TextBox.Text += "Set DateTime:Ok \r\n";
                }
                else
                {
                    HTA860Return_TextBox.Text += String.Format("Set DateTime: error! return: {0}\r\n", resultCode.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SaveToBase(string sDatumVreme, string sBrojKartice, int idUredjaja, string sDogadjaj)
        {
            if (string.IsNullOrEmpty(sBrojKartice))
            {
                return;
            }
            if (string.IsNullOrEmpty(sDatumVreme))
            {
                return;
            }

            SqlConnection con = null;
            con = new SqlConnection(this.sqlConnectionStringERV);

            {
                con.Open();

                SqlCommand comsql1 = new System.Data.SqlClient.SqlCommand(
                                       "Select IDZaposleni from vPoruka where BrojKartice = @BrojKartice", con);
                SqlParameter parBrojKartice = new SqlParameter("@BrojKartice", System.Data.SqlDbType.VarChar);
                parBrojKartice.Value = sBrojKartice;
                comsql1.Parameters.Add(parBrojKartice);

                object tmpIDZaposleni = comsql1.ExecuteScalar();

                if (tmpIDZaposleni != null)
                {
                    int IDZaposleni = Convert.ToInt32(tmpIDZaposleni);

                    SqlCommand com = new System.Data.SqlClient.SqlCommand("Insert into Dogadjaj(Datum, Vreme, Ulaz, IDTipDogadjaja, BrojKartice, IDUredjaj, IDZaposleni) " +
                                    " Values(@Datum, @Vreme, @Ulaz, @IDTipDogadjaja, @BrojKartice2, @IDUredjaj, @IDZaposleni)", con);

                    SqlParameter parDatum = new SqlParameter("@Datum", System.Data.SqlDbType.DateTime);
                    SqlParameter parVreme = new SqlParameter("@Vreme", System.Data.SqlDbType.DateTime);
                    SqlParameter parUlaz = new SqlParameter("@Ulaz", System.Data.SqlDbType.Int);
                    SqlParameter parIDTipDogadjaja = new SqlParameter("@IDTipDogadjaja", System.Data.SqlDbType.Int);
                    SqlParameter parIDZaposleni = new SqlParameter("@IDZaposleni", System.Data.SqlDbType.Int);
                    SqlParameter parBrojKartice2 = new SqlParameter("@BrojKartice2", System.Data.SqlDbType.VarChar);
                    SqlParameter parIDUredjaj = new SqlParameter("@IDUredjaj", System.Data.SqlDbType.Int);

                    parDatum.Value = UDatum(sDatumVreme);
                    parVreme.Value = UVreme(sDatumVreme);

                    if (sDogadjaj == "08")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "1";
                    }
                    if (sDogadjaj == "09")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "1";
                    }
                    if (sDogadjaj == "0A")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "2";
                    }
                    if (sDogadjaj == "0B")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "2";
                    }
                    if (sDogadjaj == "0C")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "3";
                    }
                    if (sDogadjaj == "0D")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "3";
                    }

                    parBrojKartice2.Value = sBrojKartice;
                    parIDUredjaj.Value = idUredjaja;
                    parIDZaposleni.Value = IDZaposleni;

                    com.Parameters.Add(parDatum);
                    com.Parameters.Add(parVreme);
                    com.Parameters.Add(parUlaz);
                    com.Parameters.Add(parIDTipDogadjaja);
                    com.Parameters.Add(parBrojKartice2);
                    com.Parameters.Add(parIDUredjaj);
                    com.Parameters.Add(parIDZaposleni);
                    int pog = com.ExecuteNonQuery();
                    con.Close();

                }

            }
        }
        private DateTime? UDatum(string datum)
        {

            try
            {
                string godina = datum.Substring(0, 4);
                string mesec = datum.Substring(4, 2);
                string dan = datum.Substring(6, 2);
                return new DateTime(int.Parse(godina), int.Parse(mesec), int.Parse(dan));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }
        private DateTime? UVreme(string vreme)
        {

            try
            {
                string hh = vreme.Substring(8, 2);
                string mm = vreme.Substring(10, 2);
                string ss = vreme.Substring(12, 2);
                return new DateTime(2000, 1, 1, int.Parse(hh), int.Parse(mm), int.Parse(ss));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }
    }

}
