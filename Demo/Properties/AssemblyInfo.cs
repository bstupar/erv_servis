﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ERV Hundure")]
[assembly: AssemblyDescription("komunikacioni modul za HTA")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SKY Technologies")]
[assembly: AssemblyProduct("ERV_Hundure")]
[assembly: AssemblyCopyright("Copyright © SKY Technologies 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("2fcdb906-555f-4135-ba3f-5863108a2541")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.0.2.8")]
[assembly: AssemblyFileVersion("2.0.2.8")]
[assembly: NeutralResourcesLanguageAttribute("en")]