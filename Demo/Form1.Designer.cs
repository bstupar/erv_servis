﻿namespace Demo
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.gvwHTAList = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.restartHTAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbUninterrupted = new System.Windows.Forms.CheckBox();
            this.ClearDisplay_Btn = new System.Windows.Forms.Button();
            this.startPolling_btn = new System.Windows.Forms.Button();
            this.HTA860Return_TextBox = new System.Windows.Forms.TextBox();
            this.tmrPolling = new System.Windows.Forms.Timer(this.components);
            this.clAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clDeviceId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clDeviceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clLastSeen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvwHTAList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.gvwHTAList);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(792, 279);
            this.panel1.TabIndex = 0;
            // 
            // gvwHTAList
            // 
            this.gvwHTAList.AllowUserToOrderColumns = true;
            this.gvwHTAList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvwHTAList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvwHTAList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvwHTAList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clAddress,
            this.clPort,
            this.clDeviceId,
            this.clStatus,
            this.clDeviceName,
            this.clLastSeen});
            this.gvwHTAList.ContextMenuStrip = this.contextMenuStrip1;
            this.gvwHTAList.Location = new System.Drawing.Point(0, 0);
            this.gvwHTAList.Name = "gvwHTAList";
            this.gvwHTAList.RowTemplate.Height = 23;
            this.gvwHTAList.Size = new System.Drawing.Size(789, 245);
            this.gvwHTAList.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restartHTAToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(148, 26);
            // 
            // restartHTAToolStripMenuItem
            // 
            this.restartHTAToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.restartHTAToolStripMenuItem.Name = "restartHTAToolStripMenuItem";
            this.restartHTAToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.restartHTAToolStripMenuItem.Text = "Restart device";
            this.restartHTAToolStripMenuItem.Click += new System.EventHandler(this.restartHTAToolStripMenuItem_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.cbUninterrupted);
            this.panel2.Controls.Add(this.ClearDisplay_Btn);
            this.panel2.Controls.Add(this.startPolling_btn);
            this.panel2.Controls.Add(this.HTA860Return_TextBox);
            this.panel2.Location = new System.Drawing.Point(0, 285);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(792, 251);
            this.panel2.TabIndex = 1;
            // 
            // cbUninterrupted
            // 
            this.cbUninterrupted.AutoSize = true;
            this.cbUninterrupted.Checked = true;
            this.cbUninterrupted.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUninterrupted.Location = new System.Drawing.Point(118, 57);
            this.cbUninterrupted.Name = "cbUninterrupted";
            this.cbUninterrupted.Size = new System.Drawing.Size(88, 17);
            this.cbUninterrupted.TabIndex = 36;
            this.cbUninterrupted.Text = "uninterrupted";
            this.cbUninterrupted.UseVisualStyleBackColor = true;
            this.cbUninterrupted.CheckedChanged += new System.EventHandler(this.cbUninterrupted_CheckedChanged);
            // 
            // ClearDisplay_Btn
            // 
            this.ClearDisplay_Btn.Location = new System.Drawing.Point(680, 52);
            this.ClearDisplay_Btn.Name = "ClearDisplay_Btn";
            this.ClearDisplay_Btn.Size = new System.Drawing.Size(100, 22);
            this.ClearDisplay_Btn.TabIndex = 35;
            this.ClearDisplay_Btn.Text = "Clear Display";
            this.ClearDisplay_Btn.UseVisualStyleBackColor = true;
            this.ClearDisplay_Btn.Click += new System.EventHandler(this.ClearDisplay_Btn_Click);
            // 
            // startPolling_btn
            // 
            this.startPolling_btn.Location = new System.Drawing.Point(12, 52);
            this.startPolling_btn.Name = "startPolling_btn";
            this.startPolling_btn.Size = new System.Drawing.Size(100, 22);
            this.startPolling_btn.TabIndex = 34;
            this.startPolling_btn.Text = "Start Poolling all";
            this.startPolling_btn.UseVisualStyleBackColor = true;
            this.startPolling_btn.Click += new System.EventHandler(this.startPolling_btn_Click);
            // 
            // HTA860Return_TextBox
            // 
            this.HTA860Return_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HTA860Return_TextBox.Location = new System.Drawing.Point(12, 80);
            this.HTA860Return_TextBox.Multiline = true;
            this.HTA860Return_TextBox.Name = "HTA860Return_TextBox";
            this.HTA860Return_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HTA860Return_TextBox.Size = new System.Drawing.Size(768, 157);
            this.HTA860Return_TextBox.TabIndex = 4;
            // 
            // tmrPolling
            // 
            this.tmrPolling.Interval = 60000;
            this.tmrPolling.Tick += new System.EventHandler(this.tmrPolling_Tick);
            // 
            // clAddress
            // 
            this.clAddress.HeaderText = "IP Address";
            this.clAddress.Name = "clAddress";
            // 
            // clPort
            // 
            this.clPort.HeaderText = "Port";
            this.clPort.Name = "clPort";
            // 
            // clDeviceId
            // 
            this.clDeviceId.HeaderText = "ID";
            this.clDeviceId.Name = "clDeviceId";
            // 
            // clStatus
            // 
            this.clStatus.HeaderText = "Status";
            this.clStatus.Name = "clStatus";
            this.clStatus.ReadOnly = true;
            // 
            // clDeviceName
            // 
            this.clDeviceName.HeaderText = "Device Name";
            this.clDeviceName.Name = "clDeviceName";
            this.clDeviceName.ReadOnly = true;
            // 
            // clLastSeen
            // 
            this.clLastSeen.HeaderText = "Last Seen";
            this.clLastSeen.Name = "clLastSeen";
            this.clLastSeen.ReadOnly = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 538);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ERV Hundure";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvwHTAList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView gvwHTAList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox HTA860Return_TextBox;
        private System.Windows.Forms.Button ClearDisplay_Btn;
        private System.Windows.Forms.Button startPolling_btn;
        private System.Windows.Forms.ToolStripMenuItem restartHTAToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbUninterrupted;
        private System.Windows.Forms.Timer tmrPolling;
        private System.Windows.Forms.DataGridViewTextBoxColumn clAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn clPort;
        private System.Windows.Forms.DataGridViewTextBoxColumn clDeviceId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn clDeviceName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clLastSeen;
    }
}