﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace Demo
{
    public partial class FrmMain : Form
    {
        //The DeviceID
        public int htaId = 1;
        private string sqlConnectionString = "";
        private int service = 0;
        private int pullingInterval = 600;
        private int devicePort = 4660;

        // email parameters
        private int emailPort = 25;
        private string emailHost = "";
        private string emailTo = "";
        private string emailFrom = "";
        private string emailPassword = "";
        private static System.Threading.Timer _timer;


        // for debuging
        private int debug = 0;
        private int noDatabase = 0;

        public FrmMain()
        {
            InitializeComponent();
            this.configServiceMode();
            this.configPullInterval();
            this.configDevicePort();
            this.configDBConnectionString();
            this.configDebugMode();
            this.configEmailHost();
            this.configEmailTo();
            this.configEmailFrom();
            this.configEmailPassword();

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["devices"]))
            {
                string[] devices = ConfigurationManager.AppSettings["devices"].Split(',');

                for (int i = 0; i < devices.Length - 1; i++)
                {
                    string deviceAddress = devices[i].Trim();
                    if (!String.IsNullOrEmpty(deviceAddress))
                    {
                        gvwHTAList.Rows.Add(deviceAddress, this.devicePort, this.htaId);
                        this.deviceName(i, deviceAddress);
                    }
                }

                if (this.service == 1)
                {
                    tmrPolling.Interval = pullingInterval * 1000;
                    if (String.IsNullOrEmpty(this.emailHost)
                        || String.IsNullOrEmpty(this.emailTo)
                        || String.IsNullOrEmpty(this.emailFrom)
                        || String.IsNullOrEmpty(this.emailPassword))
                    {
                        return;
                    }
                    this.setEmailReportTimer();
                }
            }
        }

        private void configDebugMode()
        {
            try
            {
                this.debug = int.Parse(ConfigurationManager.AppSettings["debugMode"]);
            }
            catch (Exception)
            {
                this.debug = 0;
            }
        }
        private void configDBConnectionString()
        {
            try
            {
                this.sqlConnectionString = ConfigurationManager.AppSettings["sqlConnectionString"];
            }
            catch (Exception)
            {
                this.sqlConnectionString = "";
            }
        }
        private void configDevicePort()
        {
            try
            {
                this.devicePort = int.Parse(ConfigurationManager.AppSettings["defaultPort"]);
            }
            catch (Exception)
            {
                this.devicePort = 4660;
            }
        }
        private void configPullInterval()
        {
            try
            {
                this.pullingInterval = int.Parse(ConfigurationManager.AppSettings["pullingInterval"]);
            }
            catch (Exception)
            {
                this.pullingInterval = 600; // 10 min
            }
        }
        private void configServiceMode()
        {
            try
            {
                this.service = int.Parse(ConfigurationManager.AppSettings["serviceMode"]);
            } catch (Exception)
            {
                this.service = 0;
            }
        }
        private void configEmailHost()
        {
            try
            {
                this.emailHost = ConfigurationManager.AppSettings["emailHost"];
            }
            catch (Exception)
            {
                this.emailHost = "";
            }
        }
        private void configEmailTo()
        {
            try
            {
                this.emailTo = ConfigurationManager.AppSettings["emailTo"];
            }
            catch (Exception)
            {
                this.emailTo = "";
            }
        }
        private void configEmailFrom()
        {
            try
            {
                this.emailFrom = ConfigurationManager.AppSettings["emailFrom"];
            }
            catch (Exception)
            {
                this.emailFrom = "";
            }
        }
        private void configEmailPassword()
        {
            try
            {
                this.emailPassword = ConfigurationManager.AppSettings["emailPassword"];
            }
            catch (Exception)
            {
                this.emailPassword = "";
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (service == 1) 
            {
                tmrPolling.Enabled = true;
            }
        }
        private int connectToDevice(string deviceAddress, int devicePort, int row)
        {
            int htaHandle = 0;
            try
            {
                int resultCode;
                resultCode = THTA830DLL.HUNHTAOpenSocket(ref htaHandle, deviceAddress, devicePort);
                if (resultCode == 0)
                {
                    gvwHTAList.Rows[row].Cells[3].ReadOnly = false;
                    gvwHTAList.Rows[row].Cells[3].Value = "Connected";
                    gvwHTAList.Rows[row].Cells[3].ReadOnly = true;
                    return htaHandle;
                }
                else
                {
                    gvwHTAList.Rows[row].Cells[3].ReadOnly = false;
                    gvwHTAList.Rows[row].Cells[3].Value = "Unable to connect";
                    gvwHTAList.Rows[row].Cells[3].ReadOnly = true;
                    return htaHandle;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return htaHandle;
            }
        }
        private void disconnectFromDevice(int htaHandle, int row)
        {
            int resultCode = -1;
            
            try
            {
                resultCode = THTA830DLL.HUNHTACloseSocket(htaHandle);
                gvwHTAList.Rows[row].Cells[3].ReadOnly = false;
                gvwHTAList.Rows[row].Cells[3].Value = "Disconnected";
                gvwHTAList.Rows[row].Cells[3].ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                gvwHTAList.Rows[row].Cells[3].ReadOnly = false;
                gvwHTAList.Rows[row].Cells[3].Value = "Disconnected";
                gvwHTAList.Rows[row].Cells[3].ReadOnly = true;
            }
        }
        private bool pingDevice(string addr)
        {
            Ping pingSender = new Ping();
            try
            {
                PingReply reply = pingSender.Send(addr.Trim(), 2000);

                return (reply.Status == IPStatus.Success);
            }
            catch (PingException)
            {
                // invalid IP address
                return false;
            }
            finally
            {
                if (pingSender != null)
                {
                    pingSender.Dispose();
                }
            }
        }
        private void pullData(int row)
        {
            string deviceAddress;
            int devicePort;
            try
            {
                deviceAddress = Convert.ToString(gvwHTAList.Rows[row].Cells[0].Value);
                devicePort = Convert.ToInt32(Convert.ToString(gvwHTAList.Rows[row].Cells[1].Value).Trim());
            }
            catch (Exception)
            {
                return;
            }

            // ping device to check if it's on the network
            if (!this.pingDevice(deviceAddress))
            {
                gvwHTAList.Rows[row].Cells[3].ReadOnly = false;
                gvwHTAList.Rows[row].Cells[3].Value = "Offline";  
                gvwHTAList.Rows[row].Cells[3].ReadOnly = true;

                if (String.IsNullOrEmpty(Convert.ToString(gvwHTAList.Rows[row].Cells[5].Value)))
                {
                    gvwHTAList.Rows[row].Cells[5].ReadOnly = false;
                    gvwHTAList.Rows[row].Cells[5].Value = DateTime.Now.ToString();
                    gvwHTAList.Rows[row].Cells[5].ReadOnly = true;
                }
                return;
            }

            if (!String.IsNullOrEmpty(Convert.ToString(gvwHTAList.Rows[row].Cells[5].Value)))
            {
                gvwHTAList.Rows[row].Cells[5].ReadOnly = false;
                gvwHTAList.Rows[row].Cells[5].Value = "";
                gvwHTAList.Rows[row].Cells[5].ReadOnly = true;
            }

            // connect to the device
            int htaHandle = this.connectToDevice(deviceAddress, devicePort, row);
            if (htaHandle == 0)
            {
                // Unable to connect
                return;
            }

            // start pulling data from the device
            int deviceID = this.IDUredjaj(deviceAddress);

            // pull - finished ? disconnect
            if (this.PovuciPodatkeSaUredjaja(htaHandle, deviceID, 0, 0))
            {
                this.PostaviTrenutnoVreme(htaHandle, deviceID);
                this.disconnectFromDevice(htaHandle, row);
                if (this.debug == 1) 
                {
                    HTA860Return_TextBox.Text += deviceAddress + " finished" + "\r\n";
                }
                return;
            } 
        }
        private void startPolling_btn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvwHTAList.Rows.Count - 1; i++)
            {
                pullData(i);
            }
        }
        private bool PovuciPodatkeSaUredjaja(int htaHandle, int deviceID, int recurson, int prevRecords)
        {
            if (recurson == 0) { 
                Thread.Sleep(1000);
            }
            else 
            { 
                Thread.Sleep(500); 
            }

            int records = 0;
            byte[] stRecord = new byte[2048];
            int returnCode = 0;
            int resultCode = -1;

            resultCode = THTA830DLL.HTA850PollingData(htaHandle, prevRecords, stRecord, ref records, ref returnCode, 10000);
            
            if (returnCode == 0 && records > 0)
            {
                for (int i = 0; i < records; i++)
                {
                    string sDatumVreme = "";
                    string sBrojKartice = "";
                    string sDogadjaj = "";
                    int tmpIndex = (i * 41);

                    StringBuilder tmpSB = new StringBuilder();
                    StringBuilder tmpSBDatumVreme = new StringBuilder();
                    StringBuilder tmpSBBrojKartice = new StringBuilder();
                    tmpSB.Append("Time:");
                    for (int j = 0; j < 20; j++)
                    {
                        if (stRecord[tmpIndex + j] == 0x00 || stRecord[tmpIndex + j] == 0xFF)
                            continue;
                        tmpSB.Append(Convert.ToChar(stRecord[tmpIndex + j]));
                        tmpSBDatumVreme.Append(Convert.ToChar(stRecord[tmpIndex + j]));
                    }
                    sDatumVreme = tmpSBDatumVreme.ToString();
                    
                    tmpSB.Append(", Reader:").Append(stRecord[tmpIndex + 20].ToString("X2"));
                    tmpSB.Append(", Input Type:").Append(stRecord[tmpIndex + 21].ToString("X2"));
                    tmpSB.Append(", Section:").Append(stRecord[tmpIndex + 22].ToString("X2"));
                    sDogadjaj = stRecord[tmpIndex + 22].ToString("X2");
                    tmpSB.Append(", Class:").Append(stRecord[tmpIndex + 23].ToString("X2"));
                    tmpSB.Append(", Event Code:").Append(stRecord[tmpIndex + 24].ToString("X2"));

                    tmpSB.Append(", Card:");
                    for (int j = 25; j < 41; j++)
                    {
                        if (stRecord[tmpIndex + j] == 0x00 || stRecord[tmpIndex + j] == 0xFF)
                            continue;
                        tmpSB.Append(Convert.ToChar(stRecord[tmpIndex + j]));
                        tmpSBBrojKartice.Append(Convert.ToChar(stRecord[tmpIndex + j]));
                    }
                    sBrojKartice = tmpSBBrojKartice.ToString();
                    if (debug == 1)
                    {
                        tmpSB.Append("\r\n");
                        //HTA860Return_TextBox.Text += "DatumVreme: "+sDatumVreme+", Kartica: "+sBrojKartice+", ID: "+id+", Dogadjaj: "+sDogadjaja;
                        HTA860Return_TextBox.Text +="\r\n" + deviceID+ " - " + sBrojKartice ;
                    }
                    SaveToBase(sDatumVreme, sBrojKartice, deviceID, sDogadjaj);
                    
                }
                return PovuciPodatkeSaUredjaja(htaHandle, deviceID, 1, records);
            }
            else
            {
                return true;
            }
        }
        private void restartHTAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //int resultCode = -1;
                //uint timeout = 3000;
                //resultCode = THTA830DLL.HUNRestartHTA(Convert.ToInt32(Convert.ToString(gvwHTAList.CurrentRow.Cells[5].Value).Trim()), Convert.ToInt32(Convert.ToString(gvwHTAList.CurrentRow.Cells[2].Value).Trim()), timeout);
                //if (resultCode == 0)
                //{
                //    MessageBox.Show("Restart HTA: OK");
                //}
                //else
                //{
                //    MessageBox.Show("Restart HTA:error!return:" + resultCode.ToString());
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PostaviTrenutnoVreme(int htaHandle, int deviceID)
        {
            try
            {
                int resultCode = -1;
                uint timeout = 1000;
                string date, time;
                date = DateTime.Now.Year.ToString().PadLeft(4, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + Convert.ToInt16(DateTime.Now.DayOfWeek).ToString();
                time = DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
                resultCode = THTA830DLL.HUNWriteHTADateTime(htaHandle, 1, date, time, timeout);
                if (this.debug == 1)
                {
                    if (resultCode == 0)
                    {
                        HTA860Return_TextBox.Text += "\r\nSet DateTime:Ok \r\n";
                    }
                    else
                    {
                        HTA860Return_TextBox.Text += String.Format("Set DateTime: error! return: {0}\r\n", resultCode.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ClearDisplay_Btn_Click(object sender, EventArgs e)
        {
            HTA860Return_TextBox.Clear();
        }
        private void tmrPolling_Tick(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    tmrPolling.Enabled = false;
                    startPolling_btn_Click(null, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            finally
            {
                tmrPolling.Enabled = true;
            }
        }
        private void SaveToBase(string sDatumVreme, string sBrojKartice, int deviceID, string sDogadjaj)
        {
            if (string.IsNullOrEmpty(sBrojKartice) || sBrojKartice == "30191000")
            {
                return;
            }
            if (string.IsNullOrEmpty(sDatumVreme))
            {
                return;
            }


            using (SqlConnection con = new SqlConnection(this.sqlConnectionString))

            {
                con.Open();

                // ID zaposlenog
                SqlCommand comsql1 = new System.Data.SqlClient.SqlCommand(
                                       "Select IDZaposleni from vPoruka where BrojKartice = @BrojKartice", con);
                SqlParameter parBrojKartice = new SqlParameter("@BrojKartice", System.Data.SqlDbType.VarChar);
                parBrojKartice.Value = sBrojKartice;
                comsql1.Parameters.Add(parBrojKartice);

                object tmpIDZaposleni = comsql1.ExecuteScalar();

                if (tmpIDZaposleni != null)
                {
                    int IDZaposleni = Convert.ToInt32(tmpIDZaposleni);

                    SqlCommand com = new System.Data.SqlClient.SqlCommand("Insert into Dogadjaj(Datum, Vreme, Ulaz, IDTipDogadjaja, BrojKartice, IDUredjaj, IDZaposleni) " +
                                    " Values(@Datum, @Vreme, @Ulaz, @IDTipDogadjaja, @BrojKartice2, @IDUredjaj, @IDZaposleni)", con);

                    SqlParameter parDatum = new SqlParameter("@Datum", System.Data.SqlDbType.DateTime);
                    SqlParameter parVreme = new SqlParameter("@Vreme", System.Data.SqlDbType.DateTime);
                    SqlParameter parUlaz = new SqlParameter("@Ulaz", System.Data.SqlDbType.Int);
                    SqlParameter parIDTipDogadjaja = new SqlParameter("@IDTipDogadjaja", System.Data.SqlDbType.Int);
                    SqlParameter parIDZaposleni = new SqlParameter("@IDZaposleni", System.Data.SqlDbType.Int);
                    SqlParameter parBrojKartice2 = new SqlParameter("@BrojKartice2", System.Data.SqlDbType.VarChar);
                    SqlParameter parIDUredjaj = new SqlParameter("@IDUredjaj", System.Data.SqlDbType.Int);

                    parDatum.Value = UDatum(sDatumVreme);
                    parVreme.Value = UVreme(sDatumVreme);
                    // F1
                    if (sDogadjaj == "08" || sDogadjaj == "00")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "1";
                    }
                    // F2
                    if (sDogadjaj == "09" || sDogadjaj == "01")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "1";
                    }
                    // F3
                    if (sDogadjaj == "0A" || sDogadjaj == "02")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "2";
                    }
                    if (sDogadjaj == "0B" || sDogadjaj == "03")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "2";
                    }
                    // F4
                    if (sDogadjaj == "0C" || sDogadjaj == "04")
                    {
                        parUlaz.Value = "0";
                        parIDTipDogadjaja.Value = "3";
                    }
                    if (sDogadjaj == "0D" || sDogadjaj == "05")
                    {
                        parUlaz.Value = "1";
                        parIDTipDogadjaja.Value = "3";
                    }

                    parBrojKartice2.Value = sBrojKartice;
                    parIDUredjaj.Value = deviceID;
                    parIDZaposleni.Value = IDZaposleni;

                    if (debug == 1)
                    {
                        //HTA860Return_TextBox.Text += "\r\n"+sIPUredjaj+" - Datum: " + parDatum.Value + ", Vreme: " + parVreme.Value + ", Ulaz: " + parUlaz.Value + ", TipDogadjaja: " + parIDTipDogadjaja.Value +", Kartica: " + parBrojKartice2.Value + ", Uredjaj: " + parIDUredjaj.Value + ", Zaposleni" + parIDZaposleni.Value;
                        //HTA860Return_TextBox.Text += "\r\n" + " - " + parBrojKartice2.Value + " - " + parIDUredjaj.Value;
                    }
                    if (noDatabase == 1)
                    {
                        con.Close();
                        return;
                    }
                    com.Parameters.Add(parDatum);
                    com.Parameters.Add(parVreme);
                    com.Parameters.Add(parUlaz);
                    com.Parameters.Add(parIDTipDogadjaja);
                    com.Parameters.Add(parBrojKartice2);
                    com.Parameters.Add(parIDUredjaj);
                    com.Parameters.Add(parIDZaposleni);
                    int pog = com.ExecuteNonQuery();
                    con.Close();

                }

            }
        }
        private int IDUredjaj(string sIPUredjaj)
        {
            
            using (SqlConnection con = new SqlConnection(this.sqlConnectionString))
            {
                con.Open();
                SqlCommand comdevice = new System.Data.SqlClient.SqlCommand(
                        "SELECT IDUredjaj from Uredjaj where IPAdresa = @IPAdresa", con);
                SqlParameter parIPAdresa = new SqlParameter("@IPAdresa", System.Data.SqlDbType.NVarChar);
                parIPAdresa.Value = sIPUredjaj;
                comdevice.Parameters.Add(parIPAdresa);
                object tmpIDUredjaj = comdevice.ExecuteScalar();
                int uredjajid = Convert.ToInt32(tmpIDUredjaj);

                if (uredjajid == 0)
                {
                    SqlCommand conAddDevice = new System.Data.SqlClient.SqlCommand(
                        "INSERT INTO Uredjaj (IDTipUredjaja, Opis, IPAdresa) values (1, @Opis, @IPAdresa); SELECT SCOPE_IDENTITY()", con);

                    SqlParameter paramOpis = new SqlParameter("@Opis", System.Data.SqlDbType.NVarChar);
                    SqlParameter paramIPAdresa = new SqlParameter("@IPAdresa", System.Data.SqlDbType.NVarChar);

                    paramOpis.Value = "Nov Uredjaj";
                    paramIPAdresa.Value = sIPUredjaj;
                    conAddDevice.Parameters.Add(paramOpis);
                    conAddDevice.Parameters.Add(paramIPAdresa);

                    object objNewDevice = conAddDevice.ExecuteScalar();
                    int newDevice = Convert.ToInt32(objNewDevice);
                    con.Close();

                    return newDevice;
                }
                con.Close();
                return uredjajid;
            }
        }
        private DateTime? UDatum(string datum)
        {
            
            try
            {
                string godina = datum.Substring(0, 4);
                string mesec = datum.Substring(4, 2);
                string dan = datum.Substring(6, 2);
                return new DateTime(int.Parse(godina), int.Parse(mesec), int.Parse(dan));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            
        }
        private DateTime? UVreme(string vreme)
        {
            
            try
            {
                string hh = vreme.Substring(8, 2);
                string mm = vreme.Substring(10, 2);
                string ss = vreme.Substring(12, 2);
                return new DateTime(2000, 1, 1, int.Parse(hh), int.Parse(mm), int.Parse(ss));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
            
        }
        private void deviceName(int row, string deviceAddress)
        {

            using (SqlConnection con = new SqlConnection(this.sqlConnectionString))
            {
                con.Open();

                SqlCommand comDeviceName = new System.Data.SqlClient.SqlCommand(
                    "SELECT Opis from Uredjaj where IPAdresa = @IPAdresa", con);
                SqlParameter parIPAdresa = new SqlParameter("@IPAdresa", System.Data.SqlDbType.NVarChar);
                parIPAdresa.Value = deviceAddress;
                comDeviceName.Parameters.Add(parIPAdresa);
                object tmpDeviceName = comDeviceName.ExecuteScalar();
                if (tmpDeviceName == null)
                {
                    gvwHTAList.Rows[row].Cells[4].ReadOnly = false;
                    gvwHTAList.Rows[row].Cells[4].Value = "Undefined device";
                    gvwHTAList.Rows[row].Cells[4].ReadOnly = true;
                    con.Close();
                    return;
                }
                string deviceName = tmpDeviceName.ToString();

                gvwHTAList.Rows[row].Cells[4].ReadOnly = false;
                gvwHTAList.Rows[row].Cells[4].Value = deviceName;
                gvwHTAList.Rows[row].Cells[4].ReadOnly = true;
                con.Close();
            }
        }
        public string errorCode(int code)
        {
            switch (code)
            {
                case 0:
                    return "Success";
                case 1125:
                    return "Error: 1125 - Overtime when operate multi-threading programs";
                case 1126:
                    return "Error: 1126 - Handle release";
                case 1001:
                    return "Error: 1001 - Sending parameter";
                case 1002:
                    return "Error: 1002 - Socket communication";
                case 1003:
                    return "Error: 1003 - Data length too short, device returned an invalid data length";
                case 1103:
                    return "Error: 1103 - Length of packet smaller than request";
                case 1004:
                    return "Error: 1004 - Invalid control handler received";
                case 1005:
                    return "Error: 1005 - Respond end char";
                case 1006:
                    return "Error: 1006 - 16-bit (CRC-16) returned";
                case 1106:
                    return "Error: 1106 - 16-bit (CRC-16) set";
                case 1007:
                    return "Error: 1007 - Wrong command or not supported function";
                case 1008:
                    return "Error: 1008 - While performing R/W to slave device";
                case 1009:
                    return "Error: 1009 - Data length transmitted exceeded max allowed length";
                case 1010:
                    return "Error: 1010 - No data was retrieved";
                case 4445:
                    return "Error: 4445 - while reading device data or records";
                case 1025:
                    return "Error: 1025 - Operation timed out during asynchronous read/write";
                case 1026:
                    return "Error: 1026 - Operation error during asynchronous read/write";
                case 2225:
                    return "Error: 2225 - Data was not retrieved during asynchronous read/write";
                default:
                    return "Error occured! Response code: " + code.ToString();
            }
        }
        private void sendEmailReport()
        {
            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient();
            client.Credentials = new NetworkCredential(this.emailFrom, this.emailPassword);
            client.Port = this.emailPort;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = this.emailHost;
            mail.To.Add(new MailAddress(this.emailTo));
            mail.From = new MailAddress(this.emailFrom);
            mail.Subject = "ERV Dnevni izveštaj za " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(DateTime.Now.ToString("dddd dd. MMMM yyyy.", CultureInfo.CreateSpecificCulture("sr")));
            string report = this.emailReport();
            Console.WriteLine(report);
            if (report.Length == 0)
            {
                report = "Svi uređaji su na mreži";
            }

            mail.Body = report;
            try
            {
                client.Send(mail);
                Console.WriteLine("Email sent");
            }
            catch (SmtpFailedRecipientException error)
            {
                Console.WriteLine("Unable to send email " + error);
                HTA860Return_TextBox.Text += "Unable to send daily report";
            }
        }
        private string emailReport()
        {
            string result = "";
            for(int i = 0; i < gvwHTAList.Rows.Count -1; i++)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(gvwHTAList.Rows[i].Cells[5].Value)))
                {
                    string deviceAddress = Convert.ToString(gvwHTAList.Rows[i].Cells[0].Value);
                    string deviceName = Convert.ToString(gvwHTAList.Rows[i].Cells[4].Value);
                    string lastSeen = Convert.ToString(gvwHTAList.Rows[i].Cells[5].Value);
                    result += deviceAddress + " " + deviceName + " je poslednji put viđen @ " + lastSeen + "\n\r";
                }
            }
            return result;
        }
        private void setEmailReportTimer()
        {
            TimeSpan timeToGo = (DateTime.Now.AddDays(1).Date - DateTime.Now).Add(new TimeSpan(8, 0, 0)); // (time till 00:00).add(8 hours) - 8h each morning
            _timer = new System.Threading.Timer(x => sendEmailReport(), null, timeToGo, new TimeSpan(1, 0, 0, 0));
        }

        private void cbUninterrupted_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}